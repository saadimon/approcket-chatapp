import { combineReducers } from "redux";
import Auth from "./auth/reducers";
import Rooms from "./rooms/reducers";
import Messages from "./messages/reducers";
import Users from "./users/reducers";
import App from "./app/reducers";

export default combineReducers({
  Auth,
  Rooms,
  Messages,
  Users,
  App,
});
