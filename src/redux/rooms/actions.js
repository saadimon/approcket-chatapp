import Data from "../../data";
import RoomService from "../../services/RoomService";
import { setActiveRoomEvent } from "../app/actions";
import { ADD_ROOM, ADD_USERS, UPDATE_LAST_MESSAGE } from "./constants";

export const createRoom = (users, name) => (dispatch, getState) =>
  new Promise((resolve) => {
    const isDirectMessage = !Array.isArray(users);
    const state = getState();
    const activeUser = Data.getUser(state);
    const usersArr = isDirectMessage
      ? [users, activeUser]
      : [...users, activeUser];

    if (isDirectMessage) {
      const rooms = Data.getRooms(state);
      const exists = Data.checkIfDMExists(rooms, users.id);
      if (exists) {
        dispatch(setActiveRoomEvent(exists.id));
        return resolve(true);
      }
    }

    const room = RoomService.createRoomObj(usersArr, name);
    RoomService.createRoom(room).then((res) => {
      if (res) {
        // dispatch(createRoomEvent(room));
        // dispatch(setActiveRoomEvent(room.id));
        resolve(true);
      } else {
        resolve(false);
      }
    });
  });

export const addUsersEvent = (room, users) => ({
  type: ADD_USERS,
  room,
  users,
});

export const createRoomEvent = (room) => ({
  type: ADD_ROOM,
  room,
});

export const updateLastMessageEvent = (message) => ({
  type: UPDATE_LAST_MESSAGE,
  message,
});
