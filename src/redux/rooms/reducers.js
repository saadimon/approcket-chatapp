import { ADD_ROOM, ADD_USERS, UPDATE_LAST_MESSAGE } from "./constants";
import Objects from "../../util/Objects";
import { LOGOUT_USER } from "../auth/constants";

const INIT_STATE = {};

const Reducer = (state = INIT_STATE, action) => {
  switch (action.type) {
    case ADD_ROOM:
      return {
        ...state,
        [action.room.id]: action.room,
      };
    case ADD_USERS:
      return {
        ...state,
        [action.room.id]: Objects.addObjectItems(
          state[action.room.id],
          "users",
          action.users
        ),
      };
    case UPDATE_LAST_MESSAGE:
      return {
        ...state,
        [action.message.room]: {
          ...state[action.message.room],
          lastMessage: action.message.text,
        },
      };
    case LOGOUT_USER:
      return INIT_STATE;
    default:
      return state;
  }
};

export default Reducer;
