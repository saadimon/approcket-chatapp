import { LOGOUT_USER } from "../auth/constants";
import { SET_ROOM, SET_INITIAL_LOADING } from "./constants";

const INIT_STATE = {};

const Reducer = (state = INIT_STATE, action) => {
  switch (action.type) {
    case SET_ROOM:
      return {
        ...state,
        activeRoom: action.payload,
      };
    case SET_INITIAL_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    case LOGOUT_USER:
      return INIT_STATE;
    default:
      return state;
  }
};

export default Reducer;
