import AuthService from "../../services/AuthService";
import RoomService from "../../services/RoomService";
import UserService from "../../services/UserService";
import Util from "../../util";
import { updateUserEvent } from "../auth/actions";
import { addMessageEvent } from "../messages/actions";
import { createRoomEvent } from "../rooms/actions";
import { addUserEvent } from "../users/actions";
import { SET_INITIAL_LOADING, SET_ROOM } from "./constants";

export const loadInitialData = () => (dispatch, getState) =>
  new Promise(async (resolve) => {
    const myUserId = getState().Auth.user?.id;
    if (!myUserId) return resolve(false);
    dispatch(setInitialLoadingEvent(true));
    const user = await UserService.getUser(myUserId);
    if (!user) return resolve(false);
    dispatch(updateUserEvent(user));
    const roomsList = user.rooms;
    const usersList = [];
    await new Promise(async (resolve) => {
      const rooms = await Promise.all(
        roomsList.map((roomId) => RoomService.getRoom(roomId))
      );

      if (rooms.length) dispatch(setActiveRoomEvent(rooms[0].id));

      for (const room of rooms) {
        if (room) {
          dispatch(createRoomEvent(room));
          const userIds = room.users;
          usersList.push(...userIds);

          const messages = await RoomService.getMessages(room.id);
          for (const message of messages) {
            dispatch(
              addMessageEvent(Util.firestoreMessageToLocalMessage(message))
            );
          }
        }
      }
      resolve();
    });
    const uniqueUsersList = [...new Set(usersList)];
    await new Promise(async (resolve) => {
      const users = await Promise.all(
        uniqueUsersList.map((userId) => {
          if (userId !== myUserId) return UserService.getUser(userId);
        })
      );
      users.forEach((user) => {
        if (user) {
          dispatch(addUserEvent(user));
        }
      });
      resolve();
    });

    dispatch(setInitialLoadingEvent(false));
    resolve(true);
  });

export const setInitialLoadingEvent = (loading) => ({
  type: SET_INITIAL_LOADING,
  payload: loading,
});

export const setActiveRoomEvent = (roomId) => ({
  type: SET_ROOM,
  payload: roomId,
});
