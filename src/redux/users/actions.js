import UserService from "../../services/UserService";
import { ADD_USER } from "./constants";

export const addUser = (userId) => (dispatch) =>
  new Promise(async (resolve) => {
    const user = await UserService.getUser(userId);
    if (!user) return resolve(false);
    dispatch(addUserEvent(user));
    resolve(true);
  });

export const addUserEvent = (user) => ({
  type: ADD_USER,
  user,
});
