import { LOGOUT_USER } from "../auth/constants";
import { ADD_USER } from "./constants";

const INIT_STATE = {};

const Reducer = (state = INIT_STATE, action) => {
  switch (action.type) {
    case ADD_USER:
      return {
        ...state,
        [action.user.id]: action.user,
      };
    case LOGOUT_USER:
      return INIT_STATE;
    default:
      return state;
  }
};

export default Reducer;
