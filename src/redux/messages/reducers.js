import { ADD_MESSAGE, DELETE_MESSAGE, UPDATE_MESSAGE } from "./constants";
import Objects from "../../util/Objects";
import { ADD_ROOM } from "../rooms/constants";
import { LOGOUT_USER } from "../auth/constants";

const INIT_STATE = {};

const Reducer = (state = INIT_STATE, action) => {
  switch (action.type) {
    case ADD_MESSAGE:
      return {
        ...state,
        [action.message.room]: [...state[action.message.room], action.message],
      };
    case ADD_ROOM:
      return {
        ...state,
        [action.room.id]: [],
      };
    case UPDATE_MESSAGE:
      return {
        ...state,
        [action.message.room]: Objects.updateItem(
          state[action.message.room],
          action.message,
          (message) => message.id == action.message.id
        ),
      };
    case DELETE_MESSAGE:
      return {
        ...state,
        [action.message.room]: Objects.removeItem(
          state[action.message.room],
          (message) => message.id == action.message.id
        ),
      };
    case LOGOUT_USER:
      return INIT_STATE;
    default:
      return state;
  }
};

export default Reducer;
