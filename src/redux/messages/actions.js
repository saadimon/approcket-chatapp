import Data from "../../data";
import MessageStatus from "../../data/enums/MessageStatus";
import MessageService from "../../services/MessageService";
import { updateLastMessageEvent } from "../rooms/actions";
import { ADD_MESSAGE, DELETE_MESSAGE, UPDATE_MESSAGE } from "./constants";

export const sendMessage = (text) => (dispatch, getState) =>
  new Promise((resolve) => {
    const state = getState();
    const activeUser = Data.getUser(state);
    const roomId = Data.getActiveRoom(state);
    if (!roomId) return resolve(false);
    const message = MessageService.createMessage(text, roomId, activeUser.id);
    dispatch(addMessageEvent({ ...message, status: MessageStatus.SENDING }));
    MessageService.sendMessage(message).then((res) => {
      if (res) {
        dispatch(
          updateMessageEvent({
            ...message,
            timeStamp: new Date(),
            status: MessageStatus.SENT,
          })
        );
        dispatch(updateLastMessageEvent(message));
      } else {
        dispatch(
          updateMessageEvent({
            ...message,
            timeStamp: new Date(),
            status: MessageStatus.ERROR_SENDING,
          })
        );
      }

      return resolve(true);
    });
  });

export const deleteMessage = (message) => (dispatch) =>
  new Promise((resolve) => {
    if (message.status == MessageStatus.ERROR_SENDING)
      dispatch(deleteMessageEvent(message));
    else {
      // todo: handle delete from firebase and then delete from redux
    }
    resolve(true);
  });

export const deleteMessageEvent = (message) => ({
  type: DELETE_MESSAGE,
  message,
});

export const updateMessage = (message) => (dispatch) =>
  new Promise((resolve) => {});

export const addMessageEvent = (message) => ({
  type: ADD_MESSAGE,
  message,
});

export const updateMessageEvent = (message) => ({
  type: UPDATE_MESSAGE,
  message,
});
