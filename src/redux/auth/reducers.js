import {
  AUTH_API_FAILED,
  AUTH_API_LOADING,
  LOGIN_USER,
  LOGOUT_USER,
  UPDATE_USER,
} from "./constants";
import { firebase } from "../../data/firebase";
import Objects from "../../util/Objects";
import { ADD_ROOM } from "../rooms/constants";

const user = firebase.auth().currentUser;

const isLoggedIn = !!user;

const INIT_STATE = {
  user: undefined,
  isLoggedIn,
  loading: false,
  loadingUser: isLoggedIn ? true : false,
};

const Reducer = (state = INIT_STATE, action) => {
  switch (action.type) {
    case ADD_ROOM:
      return {
        ...state,
        user: { ...state.user, rooms: [...state.user.rooms, action.room.id] },
      };
    case UPDATE_USER:
      return {
        ...state,
        user: Objects.merge(state.user, action.payload),
        isLoggedIn: true,
        loadingUser: false,
      };
    case LOGIN_USER:
      return {
        ...state,
        user: { ...action.user, settings: undefined },
        isLoggedIn: true,
        loading: false,
        error: null,
        loadingUser: false,
      };
    case LOGOUT_USER:
      return {
        ...state,
        loading: false,
        error: undefined,
        user: null,
        isLoggedIn: false,
        loadingUser: false,
      };
    case AUTH_API_LOADING:
      return { ...state, loading: action.loading };
    case AUTH_API_FAILED:
      return { ...state, loading: false, error: action.payload };
    default:
      return state;
  }
};

export default Reducer;
