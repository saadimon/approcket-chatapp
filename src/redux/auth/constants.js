export const LOGIN_USER = "LOGIN_USER";
export const LOGOUT_USER = "LOGOUT_USER";
export const UPDATE_USER = "UPDATE_USER";

export const AUTH_API_FAILED = "AUTH_API_FAILED";
export const AUTH_API_LOADING = "AUTH_API_LOADING";
