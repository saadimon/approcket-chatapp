import AuthService from "../../services/AuthService";
import {
  AUTH_API_FAILED,
  AUTH_API_LOADING,
  LOGIN_USER,
  LOGOUT_USER,
  UPDATE_USER,
} from "./constants";

export const signIn =
  ({ usernameOrEmail, password }) =>
  (dispatch) =>
    new Promise((resolve, reject) => {
      dispatch(apiLoading(true));
      AuthService.signIn({ usernameOrEmail, password })
        .then((user) => {
          dispatch(signInEvent(user));
          resolve(true);
        })
        .catch((e) => {
          dispatch(apiError(e));
          reject(e);
        });
    });

export const signInEvent = (user) => ({
  type: LOGIN_USER,
  user,
});

export const registerUser =
  ({ username, email, password }) =>
  (dispatch) =>
    new Promise((resolve, reject) => {
      dispatch(apiLoading(true));
      AuthService.createUser({ username, email, password })
        .then((user) => {
          dispatch(signInEvent({ ...user, newUser: true }));
          resolve(true);
        })
        .catch((e) => {
          dispatch(apiError(e));
          reject(e);
        });
    });

export const updateUsername =
  ({ username }) =>
  (dispatch) =>
    new Promise((resolve, reject) => {
      dispatch(apiLoading(true));
      AuthService.updateUsername({ username })
        .then(() => {
          dispatch(updateUserEvent({ username }));
          resolve(true);
        })
        .catch((e) => reject(e).finally(() => dispatch(apiLoading(false))));
    });

export const updateUserEvent = (user) => ({
  type: UPDATE_USER,
  user,
});

export const logoutUser = () => (dispatch) => {
  AuthService.logout();
  dispatch(logoutUserEvent());
};

export const logoutUserEvent = () => ({
  type: LOGOUT_USER,
});

export const apiLoading = (loading) => ({
  type: AUTH_API_LOADING,
  loading,
});

export const apiError = (error) => ({
  type: AUTH_API_FAILED,
  payload: error,
});

export const clearErrors = () => (dispatch) => dispatch(apiError(undefined));
