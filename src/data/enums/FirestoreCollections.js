const FirestoreCollections = {
  USERS: "users",
  ROOMS: "rooms",
  MESSAGES: "messages",
};

export default FirestoreCollections;
