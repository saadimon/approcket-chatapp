const FirestoreDocChangeType = {
  ADD: "added",
  UPDATE: "modified",
  DELETE: "removed",
};

export default FirestoreDocChangeType;
