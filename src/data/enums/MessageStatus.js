const MessageStatus = {
  SENDING: "sending",
  UPDATING: "updating",
  DELETING: "deleting",
  ERROR_SENDING: "error",
  SENT: "sent",
};

export default MessageStatus;
