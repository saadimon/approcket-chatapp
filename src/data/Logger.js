import Constants from "../util/Constants";
import logdown from "logdown";

function getLogger(tag, color) {
  const logger = logdown(tag || Constants.BRAND_NAME, { prefixColor: color });
  logger.state.isEnabled = true;
  return logger;
}

export default class Logger {
  static exception(exception, tag = undefined, context = undefined) {
    Logger.error(tag, exception);
    if (!Constants.IS_PRODUCTION) return;
  }

  static event(name, event) {
    if (!Constants.IS_PRODUCTION) return;
  }

  /**
   * Logging
   * Only for development environment
   */
  static log(tag, ...args) {
    if (!Constants.LOGS_ENABLED) return;

    getLogger(tag || "log").log(...args);
  }

  static info(tag, ...args) {
    if (!Constants.LOGS_ENABLED) return;

    getLogger(tag || "info", "#0073ff").info(...args);
  }

  static error(tag, ...args) {
    if (!Constants.LOGS_ENABLED) return;

    getLogger(tag || "error", "#FF0000").error(...args);
  }
}
