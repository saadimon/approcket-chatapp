const Data = {
  isLoggedIn: (state) => state.Auth.isLoggedIn,
  getUser: (state) => state.Auth.user,
  authLoading: (state) => state.Auth.loading,
  authError: (state) => state.Auth.error,

  getRooms: (state) => Object.values(state.Rooms),

  checkIfDMExists: (rooms, userId) => {
    for (const room of rooms) {
      if (room.isDirectMessage && room.users.includes(userId)) return room;
    }
  },
  getActiveRoom: (state) => state.App.activeRoom,
  getInitialLoading: (state) => state.App.loading,
  getUsersMap: (state) => ({
    ...state.Users,
    [state.Auth.user.id]: state.Auth.user,
  }),
  getMessages: (state) => state.Messages[state.App.activeRoom],
};

export default Data;
