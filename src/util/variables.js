const variables = {
  /** App */
  appName: "ChatRocket",

  /** Colors */
  colorPrimary: "#ffc815",
  colorSecondary: "#585858",
  colorBackground: "#292929",
  colorWhite: "#ffffff",
  colorBlack: "#000000",
  colorGray: "#cccccc",

  /** Styles */
  padding_x_small: "0.5rem",
  padding_small: "0.8rem",
  padding_medium: "1rem",
  padding_large: "1.5rem",
};

export default variables;
