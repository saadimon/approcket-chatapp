import MessageStatus from "../data/enums/MessageStatus";
import AuthService from "../services/AuthService";
import DateUtil from "./DateUtil";

const Util = {
  checkIfEmail: (email) => {
    const re =
      /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  },
  getDMName: (room, usersMap, ownUserId) => {
    const userId = ownUserId;
    const otherUserId = room.users.filter((user) => user !== userId)[0];
    return usersMap[otherUserId]?.username || "DM";
  },
  createGroupRoomName: (users) => {
    let groupName = "";
    users.forEach((user) => (groupName += user.username[0]));
    return groupName;
  },
  splitArrayInSize: (a, size) =>
    Array.from(new Array(Math.ceil(a.length / size)), (_, i) =>
      a.slice(i * size, i * size + size)
    ),
  firestoreMessageToLocalMessage: (message) => ({
    ...message,
    timeStamp: DateUtil.firestoreTimestampToDate(message.timeStamp),
    status: MessageStatus.SENT,
  }),
};

export default Util;
