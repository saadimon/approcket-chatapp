const IS_PRODUCTION = process.env.NODE_ENV === "production";

const Constants = {
  IS_PRODUCTION,
  LOGS_ENABLED: !IS_PRODUCTION,
};

export default Constants;
