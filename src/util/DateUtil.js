import moment from "moment";

const DateUtil = {
  formatDateTime: (date) => moment(date).calendar(),
  format: (date) => moment(date).format("d MMM, YYYY - h:mm A"),
  firestoreTimestampToDate: (timestamp) => timestamp.toDate(),
};

export default DateUtil;
