import uniqBy from "lodash/uniqBy";
import uniq from "lodash/uniq";
import keyBy from "lodash/keyBy";
import merge from "lodash/merge";
import set from "lodash/fp/set";
import update from "lodash/fp/update";
import mergeWith from "lodash/mergeWith";

function arrayMergeCustomizer(objValue, srcValue) {
  if (Array.isArray(objValue)) {
    return objValue.concat(srcValue);
  }
}

const Objects = {
  arrayDifferenceFrom(source, from, filterKey = "id") {
    if (!source || !from) return source;
    const fromKeys = new Set(from?.map((s) => s[filterKey]) || []);
    return source.filter((s) => !fromKeys.has(s[filterKey]));
  },
  arrayDifference(source, from) {
    const fromKeys = new Set(from);
    return source.filter((s) => !fromKeys.has(s));
  },
  uniqueBy: uniqBy, // unique(array, 'filterKey')
  unique: uniq, // unique(array, 'filterKey')
  arrayToObject: keyBy,

  filterObjects(object, key, value) {
    if (!object || !value) return object;
    const objKeys = Object.keys(object);

    const filtered = {};
    for (const objKey of objKeys) {
      const obj = object[objKey];
      if (obj && obj[key] === value) {
        filtered[objKey] = obj;
      }
    }
    return filtered;
  },

  /**
   *
   * Redux Helpers for Immutable Data
   * These functions return new references from the objects when operation is successful
   */
  merge: merge, // merge(o1, o2)
  set: set, // set("path.to.key", newValue, object)
  update: (path, value, object) => update(path, (o) => merge(o, value), object), // set("path.to.key", newValue, object)
  setItem(list, object, { filterKey = "id" } = {}) {
    const index = list?.findIndex((i) => i[filterKey] === object[filterKey]);
    if (index !== -1) {
      list[index] = object;
      return [...list];
    }
    return list;
  },
  addItem(
    list,
    value,
    {
      filterKey = "id",
      update = false,
      mergeArrays = false,
      reverse = false,
    } = {}
  ) {
    if (!list?.length) return [value];

    const index = list.findIndex((u) => u[filterKey] === value[filterKey]);
    let updated = false;
    if (index === -1) {
      if (reverse) list.unshift(value);
      else list.push(value);
      updated = true;
    } else if (update) {
      list[index] = mergeArrays
        ? mergeWith(list[index] || {}, value, arrayMergeCustomizer)
        : merge(list[index] || {}, value);
      updated = true;
    }
    return updated ? [...list] : list;
  },
  addItems(
    list,
    values,
    {
      filterKey = "id",
      update = false,
      mergeArrays = false,
      reverse = false,
    } = {}
  ) {
    if (!values) return list;
    if (!Array.isArray(values)) values = [values];
    if (!list?.length) return values;

    let updated = false;
    for (const value of values) {
      const updatedList = Objects.addItem(list, value, {
        filterKey,
        update,
        mergeArrays,
        reverse,
      });
      if (!updated) updated = updatedList !== list;
    }

    return updated ? [...list] : list;
  },
  updateItem(
    list,
    object,
    filterFn,
    { insert = false, mergeArrays = false, nestedMerge = true } = {}
  ) {
    const index = list?.findIndex(filterFn);
    let updated = false;
    if (index !== -1) {
      list[index] = mergeArrays
        ? mergeWith({ ...(list[index] || {}) }, object, arrayMergeCustomizer)
        : nestedMerge
        ? merge({ ...(list[index] || {}) }, object)
        : { ...(list[index] || {}), ...object };
      updated = true;
    } else if (insert) {
      list.push(object);
      updated = true;
    }
    return updated ? [...list] : list;
  },
  updateItems(
    list,
    values,
    { filterKey = "id", insert = false, mergeArrays = false } = {}
  ) {
    if (!values) return list;
    if (!Array.isArray(values)) values = [values];
    if (!list?.length) return values;

    let updated = false;
    for (const value of values) {
      const updatedList = Objects.updateItem(
        list,
        value,
        (item) => item[filterKey] === value[filterKey],
        {
          filterKey,
          insert,
          mergeArrays,
        }
      );
      if (!updated) updated = updatedList !== list;
    }

    return updated ? [...list] : list;
  },
  removeItem(list, filterFn) {
    if (!list?.length) return list;
    const newList = list.filter(filterFn);
    return newList.length === list.length ? list : [...newList];
  },
  mergeArrays(a, b, filterKey = "id") {
    // Merge two array with common key & insert if element doesn't exist
    const array = [];
    const obj = {};

    function x(a) {
      a.forEach((b) => {
        if (!(b[filterKey] in obj)) {
          obj[b[filterKey]] = obj[b[filterKey]] || {};
          array.push(obj[b[filterKey]]);
        }
        Object.keys(b).forEach(function (k) {
          obj[b[filterKey]][k] = b[k];
        });
      });
    }

    x(a);
    x(b);
    return array;
  },
  addObjectItem(
    object,
    key,
    value,
    { filterKey = "id", reverse = false } = {}
  ) {
    if (!object) object = {};
    return {
      ...object,
      [key]: Objects.addItem(object[key], value, {
        filterKey,
        update: true,
        reverse,
      }),
    };
  },
  addObjectItems(object, key, values, { filterKey = "id" } = {}) {
    let objectKey = [...(object[key] || [])];
    for (const value of values) {
      objectKey = Objects.addItem(objectKey, value, {
        filterKey,
        update: true,
      });
    }
    return { ...object, [key]: objectKey };
  },
  appendObjectItems(object, key, values, { reverse = false } = {}) {
    const objectKey = [...(object[key] || [])];
    return {
      ...object,
      [key]: reverse ? [...values, ...objectKey] : [...objectKey, ...values],
    };
  },
  setObjectItem: (object, key, value) =>
    object ? { ...object, [key]: value } : { [key]: value },
  updateObjectItem(
    object,
    key,
    value,
    filterFn,
    { insert = false, nestedMerge = true } = {}
  ) {
    const objectKey = object[key];
    const updated = Objects.updateItem(objectKey || [], value, filterFn, {
      insert,
      nestedMerge,
    });
    return updated === objectKey ? object : { ...object, [key]: updated };
  },
  updateObjectItemsByIds(object, key, ids, value, { filterKey = "id" } = {}) {
    const objectKey = [...(object[key] || [])];
    let updated = false;
    if (objectKey?.length) {
      for (const id of ids) {
        const index = objectKey.findIndex((user) => user[filterKey] === id);
        if (index !== -1) {
          objectKey[index] = { ...objectKey[index], ...value };
          updated = true;
        }
      }
    }
    return updated ? { ...object, [key]: objectKey } : object;
  },
  updateObjectItems(
    object,
    key,
    values,
    { filterKey = "id", insert = false } = {}
  ) {
    const objectKey = [...(object[key] || [])];
    let updated = false;
    if (objectKey?.length) {
      for (const value of values) {
        const updatedList = Objects.updateItem(
          objectKey,
          value,
          (user) => user[filterKey] === value[filterKey],
          {
            insert,
          }
        );
        if (!updated) updated = updatedList !== objectKey;
      }
    }
    return updated ? { ...object, [key]: objectKey } : object;
  },
  updateAllObjectKeyItems(object, value, filterFn) {
    // Update all matching objects in list of object fields
    let updated = false;
    for (const key in object) {
      let isUpdated = false;
      if (object.hasOwnProperty(key)) {
        const mItems = [...(object[key] || [])];
        if (mItems?.length) {
          const index = mItems.findIndex(filterFn);
          if (index !== -1) {
            mItems[index] = { ...mItems[index], ...value };
            isUpdated = true;
            updated = true;
          }
        }

        if (isUpdated) object[key] = mItems;
      }
    }
    return updated ? { ...object } : object;
  },
  deleteObjectItem(object, key, filterFn) {
    if (!object[key]) return object;
    const removed = Objects.removeItem(object[key], filterFn);
    return object[key] === removed ? object : { ...object, [key]: removed };
  },
  removeObjectKey: (object, key) => ({ ...object, [key]: undefined }),
  removeObjectKeys: (object, keys) => {
    if (!keys || !object) return object;
    if (!Array.isArray(keys)) keys = [keys];
    for (const key of keys) {
      delete object[key];
    }
    return { ...object };
  },
  updateObjectMap(object, key, itemsMap, { insert = false } = {}) {
    if ((!object && !insert) || !itemsMap) return object;

    const keys = itemsMap && Object.keys(itemsMap);
    if (!keys?.length) return object;

    if (!object) object = {};

    let updated;
    const map = object[key] || {};
    for (const key of keys) {
      if (map[key]) {
        map[key] = { ...map[key], ...itemsMap[key] };
        updated = true;
      } else if (insert) {
        map[key] = itemsMap[key];
        updated = true;
      }
    }

    return updated ? { ...object, [key]: { ...map } } : object;
  },
  deleteObjectMapKeys(object, key, itemKeys) {
    if (!object || !itemKeys) return object;
    if (!Array.isArray(itemKeys)) itemKeys = [itemKeys];
    for (const objKey of itemKeys) object[key] && delete object[key][objKey];
    return { ...object };
  },
  deleteAllObjectMapKeys(object, itemKeys) {
    if (!object || !itemKeys) return object;

    const keys = Object.keys(object);
    if (!keys?.length) return object;

    for (const key of keys) {
      object = Objects.deleteObjectMapKeys(object, key, itemKeys);
    }
    return { ...object };
  },
  deleteObjectMapKeysBy(object, key, value, filterKey) {
    if (!object) return object;
    const objectKey = object[key];
    const keys = objectKey && Object.keys(objectKey);
    if (!keys?.length) return object;

    let updated;
    for (const key of keys) {
      const obj = objectKey[key];
      if (obj && obj[filterKey] === value) {
        delete objectKey[key];
        updated = true;
      }
    }
    return updated ? { ...object } : object;
  },
  deleteAllObjectMapKeysBy(object, value, filterKey) {
    if (!object) return object;

    const keys = Object.keys(object);
    if (!keys?.length) return object;

    for (const key of keys) {
      object = Objects.deleteObjectMapKeysBy(object, key, value, filterKey);
    }
    return { ...object };
  },
};

export default Objects;
