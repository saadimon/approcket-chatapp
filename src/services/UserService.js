import FirestoreCollections from "../data/enums/FirestoreCollections";
import FirestoreService, { FirestoreHelper } from "./FirestoreService";

class UserService {
  static getUser = (userId) =>
    new Promise((resolve) => {
      FirestoreService.get(FirestoreCollections.USERS, userId).then((res) => {
        if (res) resolve(res);
        else resolve(false);
      });
    });
  static getUserSuggestions = (queryText) =>
    new Promise((resolve) => {
      FirestoreService.db
        .collection(FirestoreCollections.USERS)
        .where("usernameLowerCase", ">=", queryText)
        .where("usernameLowerCase", "<=", queryText + "\uf8ff")
        .get()
        .then((res) => resolve(FirestoreHelper.handleArrayResponse(res)))
        .catch((e) => resolve([]));
    });
}

export default UserService;
