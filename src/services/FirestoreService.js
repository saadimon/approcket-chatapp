import { firebase } from "../data/firebase";

export const FirestoreHelper = {
  handleArrayResponse(r, limit = 0) {
    const docs = r.docs?.map((it) => ({ ...it.data(), id: it.id }));
    return limit === 1 ? (docs?.length ? docs[0] : null) : docs;
  },
  handleObjectResponse(r) {
    const data = r.data();
    return data ? { id: r.id, ...data } : null;
  },
  getGroupsOfNine(list) {
    if (!list) return [];
    if (!Array.isArray(list)) list = [list];

    const groupsOfNine = [];
    let arr = [];
    for (const element of list) {
      if (arr.length < 9) {
        if (element) arr.push(element);
      } else {
        groupsOfNine.push(arr);
        arr = element ? [element] : [];
      }
    }
    if (arr.length && arr.length <= 9) groupsOfNine.push(arr);
    return groupsOfNine;
  },
};

export default class FirestoreService {
  static db = firebase.firestore();

  static get(node, id) {
    return this.db
      .collection(node)
      .doc(id)
      .get()
      .then((d) => FirestoreHelper.handleObjectResponse(d));
  }

  static getMany(node, ids) {
    if (ids.length > 10) {
      return Promise.all(ids.map((id) => FirestoreService.get(node, id))).then(
        (r) => FirestoreHelper.handleArrayResponse({ docs: r.filter(Boolean) })
      );
    }

    return this.db
      .collection(node)
      .where(firebase.firestore.FieldPath.documentId(), "in", ids)
      .get()
      .then((r) => FirestoreHelper.handleArrayResponse(r));
  }

  static add(node, document) {
    document = {
      ...document,
      createdAt: firebase.firestore.FieldValue.serverTimestamp(),
      updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
    };
    return document.id
      ? this.db.collection(node).doc(document.id).set(document)
      : this.db.collection(node).add(document);
  }

  static update(node, id, document) {
    return this.db
      .collection(node)
      .doc(id)
      .update({
        ...document,
        updatedAt: firebase.firestore.FieldValue.serverTimestamp(),
      });
  }

  static delete(node, id) {
    return this.db.collection(node).doc(id).delete();
  }

  /** Key value search */
  static filter(node, key, value, limit = 0) {
    return this.db
      .collection(node)
      .where(key, "==", value)
      .limit(limit)
      .get()
      .then((r) => FirestoreHelper.handleArrayResponse(r, limit));
  }

  /** Array search */
  static contains(node, key, value, limit = 0) {
    return this.db
      .collection(node)
      .where(
        key,
        typeof value === "string" ? "array-contains" : "array-contains-any",
        value
      )
      .get()
      .then((r) => FirestoreHelper.handleArrayResponse(r, limit));
  }
}
