import FirestoreCollections from "../data/enums/FirestoreCollections";
import { firebase } from "../data/firebase";
import Util from "../util";
import FirestoreService from "./FirestoreService";

export const AuthErrors = {
  USER_NOT_FOUND: "Auth/user-not-found",
  USERNAME_UNAVAILABLE: "Auth/username-unavailable",
};

const handleError = (e) => e.code || e;

const signInWithEmail = (email, password) =>
  firebase.auth().signInWithEmailAndPassword(email, password);

const usernameExists = (username) =>
  FirestoreService.filter(
    FirestoreCollections.USERS,
    "usernameLowerCase",
    username,
    1
  );

const createUser = (id, username, email) => ({
  username,
  usernameLowerCase: username.toLowerCase(),
  email,
  id,
  rooms: [],
});

export default class AuthService {
  static getUserId = firebase.auth().currentUser?.uid;
  static signIn = ({ usernameOrEmail, password }) =>
    new Promise(async (resolve, reject) => {
      const isEmail = Util.checkIfEmail(usernameOrEmail);
      let email = "";
      if (isEmail) {
        email = usernameOrEmail;
      } else {
        const username = usernameOrEmail.toLowerCase();
        const user = await usernameExists(username);
        if (!user) return reject(AuthErrors.USER_NOT_FOUND);
        email = user.email;
      }
      signInWithEmail(email, password)
        .then((userCredential) =>
          FirestoreService.get(
            FirestoreCollections.USERS,
            userCredential.user.uid
          )
        )
        .then((user) => resolve(user))
        .catch((e) => reject(handleError(e)));
    });
  static createUser = ({ username, email, password }) =>
    new Promise(async (resolve, reject) => {
      const user = await usernameExists(username.toLowerCase());
      if (user) return reject(AuthErrors.USERNAME_UNAVAILABLE);
      firebase
        .auth()
        .createUserWithEmailAndPassword(email, password)
        .then(async (userCredential) => {
          const userId = userCredential.user.uid;
          await FirestoreService.add(
            FirestoreCollections.USERS,
            createUser(userId, username, email, password)
          );
          return FirestoreService.get(FirestoreCollections.USERS, userId);
        })
        .then((user) => {
          resolve(user);
        })
        .catch((e) => reject(handleError(e)));
    });

  static updateUsername = ({ username }) =>
    new Promise(async (resolve, reject) => {
      const user = await usernameExists(username);
      if (user) return reject(AuthErrors.USERNAME_UNAVAILABLE);
      FirestoreService.update(FirestoreCollections.USERS, this.getUserId(), {
        username,
        usernameLowerCase: username,
      })
        .then(() => resolve(true))
        .catch((e) => reject(handleError(e)));
    });

  static logout = () => {
    firebase.auth();
  };
}
