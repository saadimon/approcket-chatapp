import FirestoreCollections from "../data/enums/FirestoreCollections";
import FirestoreService, { FirestoreHelper } from "./FirestoreService";
import { firebase } from "../data/firebase";

class RoomService {
  static createRoomObj = (users, name) => {
    const isDirectMessage = users.length == 2;
    const room = {
      id: FirestoreService.db.collection(FirestoreCollections.ROOMS).doc().id,
      users: users.map((user) => user.id),
      isDirectMessage,
      lastUpdate: new Date(),
      lastMessage: "Chat created. Say hi!",
    };
    if (!isDirectMessage) room.name = name;
    return room;
  };

  /** Maximum of 19 users due to firebase limitations -- In the case of batch */
  static createRoom = (room) =>
    new Promise(async (resolve) => {
      try {
        await FirestoreService.add(FirestoreCollections.ROOMS, room);
        await Promise.all(
          room.users.map((userId) =>
            FirestoreService.update(FirestoreCollections.USERS, userId, {
              rooms: firebase.firestore.FieldValue.arrayUnion(room.id),
            })
          )
        );
        resolve(true);
      } catch (e) {
        console.error(e);
        resolve(false);
      }

      // const batch = FirestoreService.db.batch();
      // batch.set(
      //   FirestoreService.db.collection(FirestoreCollections.ROOMS).doc(room.id),
      //   room
      // );
      // room.users.forEach((user) =>
      //   batch.update(
      //     FirestoreService.db
      //       .collection(FirestoreCollections.USERS)
      //       .doc(user.id),
      //     { rooms: firebase.firestore.FieldValue.arrayUnion(room.id) }
      //   )
      // );
      // batch
      //   .commit()
      //   .then(() => resolve(true))
      //   .catch((e) => {
      //     console.error(e);
      //     resolve(false);
      //   });
    });

  /** Maximum of 19 users due to firebase limitations */
  static addUsers = (room, users) =>
    new Promise((resolve) => {
      const isArray = Array.isArray(users);
      const batch = FirestoreService.db.batch();
      batch.update(
        FirestoreService.db.collection(FirestoreCollections.ROOMS).doc(room.id),
        {
          users: firebase.firestore.FieldValue.arrayUnion(
            isArray ? users : [users]
          ),
        }
      );

      if (isArray) {
        users.forEach((user) =>
          batch.update(
            FirestoreService.db
              .collection(FirestoreCollections.USERS)
              .doc(user.id),
            { rooms: firebase.firestore.FieldValue.arrayUnion([room.id]) }
          )
        );
      } else {
        batch.update(
          FirestoreService.db
            .collection(FirestoreCollections.USERS)
            .doc(users.id),
          { rooms: firebase.firestore.FieldValue.arrayUnion([room.id]) }
        );
      }
      batch
        .commit()
        .then(() => resolve(true))
        .catch((e) => {
          console.log(e);
          resolve(false);
        });
    });

  static getRoom = (roomId) =>
    new Promise(async (resolve) => {
      const room = await FirestoreService.get(
        FirestoreCollections.ROOMS,
        roomId
      );
      if (room) return resolve(room);
      else return resolve(false);
    });

  static getMessages = (roomId) =>
    new Promise(async (resolve) => {
      try {
        const messages = FirestoreHelper.handleArrayResponse(
          await FirestoreService.db
            .collection(FirestoreCollections.MESSAGES)
            .where("room", "==", roomId)
            .orderBy("timeStamp")
            .get()
        );
        resolve(messages);
      } catch (e) {
        console.error(e);
        resolve([]);
      }
    });
}

export default RoomService;
