import FirestoreCollections from "../data/enums/FirestoreCollections";
import AuthService from "./AuthService";
import FirestoreService from "./FirestoreService";
import { firebase } from "../data/firebase";
class MessageService {
  static createMessage = (text, roomId, senderId) => ({
    sender: senderId,
    room: roomId,
    text,
    id: FirestoreService.db.collection(FirestoreCollections.MESSAGES).doc().id,
    timeStamp: firebase.firestore.FieldValue.serverTimestamp(),
  });
  static sendMessage = (message) =>
    new Promise(async (resolve) => {
      try {
        const added = await FirestoreService.add(
          FirestoreCollections.MESSAGES,
          message
        );
        const roomUpdated = await FirestoreService.update(
          FirestoreCollections.ROOMS,
          message.room,
          { lastMessage: message.text }
        );
        resolve(true);
      } catch (e) {
        return resolve(false);
      }
      // const batch = FirestoreService.db.batch();
      // batch.set(
      //   FirestoreService.db
      //     .collection(FirestoreCollections.MESSAGES)
      //     .doc(message.id),
      //   message
      // );
      // batch.update(
      //   FirestoreService.db
      //     .collection(FirestoreCollections.ROOMS)
      //     .doc(message.room),
      //   {
      //     lastMessage: message.text,
      //   }
      // );
      // batch
      //   .commit()
      //   .then((res) => resolve(true))
      //   .catch((e) => {
      //     console.log(e);
      //     resolve(false);
      //   });
    });

  static deleteMessage = (messageId) =>
    new Promise((resolve) => {
      FirestoreService.delete(FirestoreCollections.MESSAGES, messageId)
        .then((res) => resolve(true))
        .catch((e) => resolve(false));
    });
}

export default MessageService;
