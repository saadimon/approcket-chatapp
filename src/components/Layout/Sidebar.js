import React from "react";
import { connect } from "react-redux";
import Data from "../../data";
import { logoutUser } from "../../redux/auth/actions";
import variables from "../../util/variables";
import Button from "../Small/Button";
import ChatRoom from "./ChatRoom";
import Logo from "./Logo";
import SearchUsers from "./Search/SearchUsers";

function Sidebar({ logout, rooms }) {
  return (
    <div
      className="vh-100"
      style={{
        flex: 1,
        backgroundColor: variables.colorBackground,
        borderRight: `2px solid ${variables.colorSecondary}`,
      }}
    >
      <div>
        <Logo />
      </div>
      <div style={{ margin: variables.padding_x_small }}>
        <SearchUsers />
      </div>
      <div style={{ flex: 1 }} className="scrollable-y">
        {rooms.map((room, i) => (
          <ChatRoom room={room} key={i} />
        ))}
      </div>
      <div
        className="center-items"
        style={{ padding: variables.padding_medium }}
      >
        <Button onClick={logout}>Logout</Button>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  rooms: Data.getRooms(state),
});

const mapDispatchToProps = (dispatch) => ({
  logout: () => dispatch(logoutUser()),
});

export default connect(mapStateToProps, mapDispatchToProps)(Sidebar);
