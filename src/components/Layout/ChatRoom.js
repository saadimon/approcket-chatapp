import React from "react";
import { connect } from "react-redux";
import Data from "../../data";
import { setActiveRoomEvent } from "../../redux/app/actions";
import Util from "../../util";
import variables from "../../util/variables";
import Avatar from "../Small/Avatar";

function ChatRoom({ room, activeRoom, setActiveRoom, usersMap, activeUser }) {
  const { isDirectMessage } = room;
  const isActiveRoom = room.id == activeRoom;
  const name = isDirectMessage
    ? Util.getDMName(room, usersMap, activeUser.id)
    : room.name;
  return (
    <div
      style={{
        padding: "20px 10px",
        borderBottom: "1px solid #777",
        flexDirection: "row",
        backgroundColor: isActiveRoom ? variables.colorPrimary : "transparent",
      }}
      className="clickable"
      onClick={(e) => (isActiveRoom ? {} : setActiveRoom(room.id))}
    >
      <div
        style={{
          marginRight: variables.padding_small,
        }}
      >
        <Avatar invert={isActiveRoom} name={name} />
      </div>
      <div>
        <h3
          style={{ color: isActiveRoom ? variables.colorBlack : "" }}
          className=" no-margin"
        >
          {name}
        </h3>
        <p
          style={{ color: isActiveRoom ? variables.colorBlack : "" }}
          className="sub-text no-margin"
        >
          {room.lastMessage}
        </p>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  activeRoom: Data.getActiveRoom(state),
  usersMap: Data.getUsersMap(state),
  activeUser: Data.getUser(state),
});

const mapDispatchToProps = (dispatch) => ({
  setActiveRoom: (...args) => dispatch(setActiveRoomEvent(...args)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChatRoom);
