import React from "react";
import LogoIcon from "../../assets/icons/logo.svg";
import variables from "../../util/variables";

function Logo({ size = 100 }) {
  return (
    <div
      style={{ padding: 20, backgroundColor: variables.colorSecondary }}
      className="center-items"
    >
      <img
        src={LogoIcon}
        alt="ChatRocket-Logo"
        style={{ width: size, height: size }}
      />
      <h2 className="no-margin" style={{ color: variables.colorPrimary }}>
        {variables.appName}
      </h2>
    </div>
  );
}

export default Logo;
