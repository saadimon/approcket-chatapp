import _ from "lodash";
import React from "react";
import { useState } from "react";
import AuthService from "../../../services/AuthService";
import variables from "../../../util/variables";
import Button from "../../Small/Button";
import TextInput from "../../Small/TextInput";
import { createRoom } from "../../../redux/rooms/actions";
import { connect } from "react-redux";
import Util from "../../../util";
import Data from "../../../data";
import UserService from "../../../services/UserService";

const users = {
  user1: {
    id: "user1",
    name: "user1",
  },
  user2: {
    id: "user2",
    name: "user2",
  },
  user3: {
    id: "user3",
    name: "user3",
  },
  user4: {
    id: "user4",
    name: "user4",
  },
  ownUser: {
    id: AuthService.getUserId,
    name: "you",
  },
};

const UserSuggestion = ({ user, onClick, selected }) => {
  return (
    <div
      onClick={onClick}
      className="clickable center-items"
      style={{
        flexDirection: "row",
        borderRadius: variables.padding_x_small,
        border: `1px solid ${
          selected ? variables.colorPrimary : variables.colorSecondary
        }`,
        margin: "3px 0",
      }}
    >
      <p>{user.username}</p>
    </div>
  );
};

function SearchModalContent({ createRoom, user: activeUser, closeModal }) {
  const [search, setSearch] = useState("");
  const [suggestions, setSuggestions] = useState([]);
  const [selectedUsers, setSelectedUsers] = useState([]);
  const [loading, setLoading] = useState(false);

  const onSubmit = async () => {
    if (loading) return;
    setLoading(true);
    if (selectedUsers.length) {
      if (selectedUsers.length > 1) {
        await createRoom(
          selectedUsers,
          Util.createGroupRoomName([...selectedUsers, activeUser])
        );
      } else {
        await createRoom(selectedUsers[0]);
      }
    }
    setLoading(false);
    setSelectedUsers([]);
    closeModal();
  };

  const onSearch = (e) => {
    const searchVal = e.target.value;
    setSearch(searchVal);
    if (searchVal) _.debounce(() => getSuggestions(searchVal), 500)();
    else setSuggestions([]);
  };

  const getSuggestions = async (search) => {
    const suggestions = await UserService.getUserSuggestions(search);
    setSuggestions(suggestions);
  };

  return (
    <div style={{ marginBottom: variables.padding_small }}>
      <h3 className="no-margin">Create new message</h3>
      <TextInput
        value={search}
        onChange={onSearch}
        noPadding
        style={{ fontSize: 24 }}
      />
      <div className={"scrollable-y"} style={{ maxHeight: "50vh" }}>
        {selectedUsers?.length > 0 && (
          <div>
            <p>Selected users:</p>
            {selectedUsers.map((user, i) => (
              <UserSuggestion
                key={i}
                user={user}
                selected
                onClick={() =>
                  setSelectedUsers(
                    selectedUsers.filter((suser) => suser.id !== user.id)
                  )
                }
              />
            ))}
          </div>
        )}
        {suggestions?.length > 0 &&
          suggestions.map((user, i) => {
            if (
              !selectedUsers.map((suser) => suser.id).includes(user.id) &&
              user.id !== activeUser.id
            )
              return (
                <UserSuggestion
                  onClick={() => setSelectedUsers([...selectedUsers, user])}
                  key={i}
                  user={user}
                />
              );
          })}
        {selectedUsers.length > 0 && (
          <Button onClick={onSubmit}>Start chat</Button>
        )}
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => ({
  createRoom: (...args) => dispatch(createRoom(...args)),
});

const mapStateToProps = (state) => ({
  user: Data.getUser(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(SearchModalContent);
