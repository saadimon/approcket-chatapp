import React from "react";
import { useState } from "react";
import Button from "../../Small/Button";
import CustomModal from "../../Small/CustomModal";
import SearchModalContent from "./SearchModalContent";

function SearchUsers() {
  const [modalVisible, setModalVisible] = useState(false);

  return (
    <div>
      <Button onClick={(e) => setModalVisible(true)}>New Chat</Button>
      <CustomModal visible={modalVisible} setVisible={setModalVisible}>
        <SearchModalContent closeModal={() => setModalVisible(false)} />
      </CustomModal>
    </div>
  );
}

export default SearchUsers;
