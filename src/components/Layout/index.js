import React from "react";
import Sidebar from "./Sidebar";

function LayoutContainer({ children, ...props }) {
  return (
    <div style={{ flexDirection: "row", flex: 1 }}>
      <div style={{ width: "25vw", maxWidth: 400 }}>
        <Sidebar />
      </div>
      <div style={{ flex: 1 }}>{children}</div>
    </div>
  );
}

export default LayoutContainer;
