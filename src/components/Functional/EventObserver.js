import React from "react";
import { useState } from "react";
import { useEffect } from "react";
import { connect } from "react-redux";
import Data from "../../data";
import FirestoreCollections from "../../data/enums/FirestoreCollections";
import FirestoreDocChangeType from "../../data/enums/FirestoreDocChangeType";
import { addMessageEvent } from "../../redux/messages/actions";
import {
  createRoomEvent,
  updateLastMessageEvent,
} from "../../redux/rooms/actions";
import { addUser } from "../../redux/users/actions";
import FirestoreService, {
  FirestoreHelper,
} from "../../services/FirestoreService";
import RoomService from "../../services/RoomService";
import Util from "../../util";
import Objects from "../../util/Objects";

function EventObserver({
  activeUser,
  rooms,
  addMessage,
  addRoom,
  usersMap,
  addUser,
}) {
  const [date, setDate] = useState(new Date());

  /** Listen to user for new rooms (and other changes in the future) */
  useEffect(() => {
    if (activeUser) {
      const unsub = FirestoreService.db
        .collection(FirestoreCollections.USERS)
        .doc(activeUser.id)
        .onSnapshot((snap) => {
          /** Handle user changes */
          if (snap.metadata.hasPendingWrites) return;
          const user = FirestoreHelper.handleObjectResponse(snap);
          const roomIds = user.rooms;
          const newRoomsIds = Objects.arrayDifference(
            roomIds,
            activeUser.rooms
          );
          newRoomsIds.forEach(async (roomId) => {
            const room = await RoomService.getRoom(roomId);
            if (room) {
              for (const userId of room.users) {
                if (!usersMap[userId]) {
                  await addUser(userId);
                }
              }
              addRoom(room);
            }
          });
        });
      return unsub;
    }
  }, []);

  /**Listen to messages in rooms*/
  useEffect(() => {
    if (Array.isArray(rooms)) {
      const groupsOfNine = Util.splitArrayInSize(rooms, 9);
      const listeners = [];
      for (const group of groupsOfNine) {
        const unsub = FirestoreService.db
          .collection(FirestoreCollections.MESSAGES)
          .where(
            "room",
            "in",
            group.map((groupObj) => groupObj.id)
          )
          .onSnapshot((snap) => {
            /** Handle new messages */
            snap.docChanges().forEach((change) => {
              if (change.type == FirestoreDocChangeType.ADD) {
                if (change.doc.metadata.hasPendingWrites) return;
                const message = Util.firestoreMessageToLocalMessage(
                  FirestoreHelper.handleObjectResponse(change.doc)
                );
                if (
                  message.sender !== activeUser.id &&
                  date < message.timeStamp
                )
                  addMessage(message);
              }
            });
          });
        listeners.push(unsub);
      }
      return listeners;
    }
  }, []);

  return <></>;
}

const mapStateToProps = (state) => ({
  activeUser: Data.getUser(state),
  rooms: Data.getRooms(state),
  usersMap: Data.getUsersMap(state),
});
const mapDispatchToProps = (dispatch) => ({
  addMessage: (...args) => {
    dispatch(addMessageEvent(...args));
    dispatch(updateLastMessageEvent(...args));
  },
  addRoom: (...args) => dispatch(createRoomEvent(...args)),
  addUser: (...args) => dispatch(addUser(...args)),
});

export default connect(mapStateToProps, mapDispatchToProps)(EventObserver);
