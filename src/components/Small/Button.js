import React from "react";
import PropTypes from "prop-types";

export const ButtonLevels = {
  PRIMARY: "primary",
  SECONDARY: "secondary",
};

function Button({
  type,
  onClick,
  children,
  level = ButtonLevels.SECONDARY,
  className,
  disabled,
  ...props
}) {
  return (
    <button
      className={`${level} ${className} ${disabled ? "disabled" : ""}`}
      onClick={(e) => {
        e.preventDefault();
        if (onClick && !disabled) onClick(e);
      }}
      {...props}
    >
      {children}
    </button>
  );
}

Button.prototypes = {
  level: PropTypes.oneOf([ButtonLevels.PRIMARY, ButtonLevels.SECONDARY]),
};

export default Button;
