import React from "react";
import ReactModal from "react-modal";
import variables from "../../util/variables";

function CustomModal({ visible, setVisible, children }) {
  const onCloseModal = (e) => setVisible(false);
  return (
    <ReactModal
      ariaHideApp={false}
      shouldCloseOnEsc={true}
      shouldCloseOnOverlayClick={true}
      onRequestClose={onCloseModal}
      isOpen={visible}
      style={{
        content: {
          width: "30vw",
          position: "initial",
          backgroundColor: variables.colorBackground,
          border: "none",
        },
        overlay: {
          display: "flex",
          justifyContent: "center",
          alignItems: "center",
          backgroundColor: "#00000080",
        },
      }}
    >
      {children}
    </ReactModal>
  );
}

export default CustomModal;
