import React from "react";
import variables from "../../util/variables";

function Avatar({
  name,
  size = 50,
  backgroundColor = variables.colorPrimary,
  textColor = variables.colorSecondary,
  invert,
}) {
  const initial = name[0];

  return (
    <div
      style={{
        borderRadius: 999,
        width: size,
        height: size,
        backgroundColor: invert ? textColor : backgroundColor,
      }}
      className="center-items"
    >
      <p
        style={{
          textTransform: "uppercase",
          fontWeight: "700",
          fontSize: 24,
          color: invert ? backgroundColor : textColor,
        }}
      >
        {initial}
      </p>
    </div>
  );
}

export default Avatar;
