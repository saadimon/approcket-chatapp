import React from "react";
import variables from "../../util/variables";

function TextInput({ label, password, noPadding, ...props }) {
  return (
    <div style={{ padding: noPadding ? 0 : `${variables.padding_x_small} 0` }}>
      <p className="label">{label}</p>
      <input type={password ? "password" : "text"} {...props} />
    </div>
  );
}

export default TextInput;
