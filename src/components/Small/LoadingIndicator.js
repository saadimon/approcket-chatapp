import React from "react";
import LoadingGif from "../../assets/images/loading.gif";

function LoadingIndicator({ size = 80 }) {
  return <img src={LoadingGif} style={{ width: size, height: size }} />;
}

export default LoadingIndicator;
