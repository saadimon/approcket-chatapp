import React from "react";
import variables from "../../../util/variables";
import EventObserver from "../../Functional/EventObserver";
import ChatBox from "./Chat/ChatBox";
import ChatInput from "./Chat/ChatInput";

function Home({}) {
  return (
    <div
      className="vh-100"
      style={{ backgroundColor: variables.colorBackground, flex: 1 }}
    >
      <EventObserver />
      <ChatBox />
      <ChatInput />
    </div>
  );
}

export default Home;
