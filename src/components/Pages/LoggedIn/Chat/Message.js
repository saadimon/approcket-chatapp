import React from "react";
import { connect } from "react-redux";
import Data from "../../../../data";
import MessageStatus from "../../../../data/enums/MessageStatus";
import AuthService from "../../../../services/AuthService";
import DateUtil from "../../../../util/DateUtil";
import variables from "../../../../util/variables";
import Avatar from "../../../Small/Avatar";
import LoadingIndicator from "../../../Small/LoadingIndicator";

function Message({ message, usersMap }) {
  const isOwnMessage = message.sender === AuthService.getUserId;

  const sender = usersMap[message.sender];

  return (
    <div style={{ flexDirection: "row", alignItems: "flex-start" }}>
      <div
        style={{
          marginLeft: variables.padding_small,
          marginRight: variables.padding_x_small,
        }}
      >
        <Avatar invert={!isOwnMessage} name={sender.username} />
      </div>
      <div className="message-container">
        <div className="arrow">
          <div className="outer"></div>
          <div className="inner"></div>
        </div>
        <div className="message-body">
          <div style={{ flexDirection: "row", alignItems: "flex-end" }}>
            <p style={{ fontWeight: "bold" }} className="dark">
              {sender.username}
            </p>
            {message.status == MessageStatus.SENT && (
              <p
                style={{ marginLeft: variables.padding_x_small }}
                className="sub-text"
              >
                {DateUtil.formatDateTime(message.timeStamp)}
              </p>
            )}
            {message.status == MessageStatus.SENDING && (
              <div style={{ marginLeft: variables.padding_x_small }}>
                <LoadingIndicator size={12} />
              </div>
            )}
            {message.status == MessageStatus.ERROR_SENDING && (
              <div style={{ marginLeft: variables.padding_x_small }}>
                <p style={{ color: "red" }}>Error</p>
              </div>
            )}
          </div>
          <p className="dark">{message.text}</p>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  usersMap: Data.getUsersMap(state),
});

export default connect(mapStateToProps)(Message);
