import React from "react";
import { useState } from "react";
import { connect } from "react-redux";
import { sendMessage } from "../../../../redux/messages/actions";
import variables from "../../../../util/variables";
import Button, { ButtonLevels } from "../../../Small/Button";

function ChatInput({ sendMessage }) {
  const [text, setText] = useState("");

  const onSubmit = () => {
    // onSend({ ...MessageService.createMessage(text, "noId"), sender: ownUser });
    sendMessage(text);
    setText("");
  };
  return (
    <div style={{ flexDirection: "row" }}>
      <form style={{ display: "flex", flex: 1 }}>
        <div style={{ flex: 1 }}>
          <input
            value={text}
            onChange={(e) => setText(e.target.value)}
            style={{ fontSize: 20, height: "100%" }}
            type="text"
          />
        </div>
        <Button
          onClick={onSubmit}
          level={ButtonLevels.PRIMARY}
          style={{
            padding: variables.padding_small,
            borderRadius: 0,
          }}
          className="no-margin"
        >
          Send
        </Button>
      </form>
    </div>
  );
}

const mapStateToProps = (state) => ({});
const mapDispatchToProps = (dispatch) => ({
  sendMessage: (...args) => dispatch(sendMessage(...args)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ChatInput);
