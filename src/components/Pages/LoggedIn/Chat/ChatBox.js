import React, { useEffect } from "react";
import { useRef } from "react";
import { connect } from "react-redux";
import Data from "../../../../data";
import LoadingIndicator from "../../../Small/LoadingIndicator";
import Message from "./Message";

function ChatBox({ messages, activeRoom }) {
  const messagesEndRef = useRef();
  const firstUpdate = useRef(true);

  useEffect(() => {
    if (firstUpdate.current) {
      messagesEndRef.current.scrollIntoView();
      firstUpdate.current = false;
    } else {
      messagesEndRef.current.scrollIntoView({ behavior: "smooth" });
    }
  }, [messages]);

  const loadingMessages = !messages;

  return (
    <div className="scrollable-y" style={{ flex: 1 }}>
      {loadingMessages ? (
        activeRoom ? (
          <div style={{ flex: 1 }} className="center-items">
            <LoadingIndicator size={40} />
          </div>
        ) : (
          <div style={{ flex: 1 }}></div>
        )
      ) : (
        (messages || []).map((message, index) => (
          <Message key={index} message={message} />
        ))
      )}
      <div ref={messagesEndRef} />
    </div>
  );
}

const mapStateToProps = (state) => ({
  messages: Data.getMessages(state),
  activeRoom: Data.getActiveRoom(state),
});

export default connect(mapStateToProps)(ChatBox);
