import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Data from "../../../data";
import Links from "../../../navigation/links";
import { clearErrors, registerUser } from "../../../redux/auth/actions";
import variables from "../../../util/variables";
import Button, { ButtonLevels } from "../../Small/Button";
import TextInput from "../../Small/TextInput";

function Register({ register, loading, error, clearErrors }) {
  const [username, setUsername] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const onRegister = () => {
    register({ username, email, password })
      .then(() => {
        setEmail("");
        setPassword("");
        setUsername("");
      })
      .catch((e) => {});
  };

  useEffect(() => {
    clearErrors();
  }, []);

  return (
    <div
      className="vh-100 center-items"
      style={{ backgroundColor: variables.colorGray }}
    >
      <div className="form-container" style={{ width: "40%" }}>
        <div>
          <h1>Register</h1>
        </div>
        <form>
          <TextInput
            value={username}
            onChange={(e) => setUsername(e.target.value)}
            name="username"
            label="Username"
          />
          <TextInput
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            name="email"
            label="Email"
          />
          <TextInput
            value={password}
            password
            onChange={(e) => setPassword(e.target.value)}
            name="password"
            label="Password"
          />
          <div
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              marginTop: variables.padding_x_small,
            }}
          >
            <Link to={Links.LOGIN}>
              <p>Login instead?</p>
            </Link>
            <Button
              disabled={loading}
              level={ButtonLevels.PRIMARY}
              type="submit"
              onClick={onRegister}
            >
              Register
            </Button>
          </div>
          {error && (
            <div>
              <p className="error">{error}</p>
            </div>
          )}
        </form>
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => ({
  register: (...args) => dispatch(registerUser(...args)),
  clearErrors: () => dispatch(clearErrors()),
});

const mapStateToProps = (state) => ({
  loading: Data.authLoading(state),
  error: Data.authError(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(Register);
