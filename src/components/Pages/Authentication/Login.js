import React from "react";
import { useEffect } from "react";
import { useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import Data from "../../../data";
import Links from "../../../navigation/links";
import { clearErrors, signIn } from "../../../redux/auth/actions";
import variables from "../../../util/variables";
import Button, { ButtonLevels } from "../../Small/Button";
import TextInput from "../../Small/TextInput";

function Login({ loading, error, signIn, clearErrors }) {
  const [usernameOrEmail, setUsernameOrEmail] = useState("");
  const [password, setPassword] = useState("");

  const onSignIn = async () => {
    signIn({ usernameOrEmail, password })
      .then(() => {
        setUsernameOrEmail("");
        setPassword("");
      })
      .catch((e) => {});
  };

  useEffect(() => {
    clearErrors();
  }, []);

  return (
    <div
      className="vh-100 center-items"
      style={{ backgroundColor: variables.colorGray }}
    >
      <div className="form-container" style={{ width: "40%" }}>
        <div>
          <h1>Sign In</h1>
        </div>
        <form>
          <TextInput
            value={usernameOrEmail}
            onChange={(e) => setUsernameOrEmail(e.target.value)}
            name="email"
            label="Username or Email"
          />
          <TextInput
            password
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            name="password"
            label="Password"
          />
          <div
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
              marginTop: variables.padding_x_small,
            }}
          >
            <Link to={Links.REGISTER}>
              <p>Create an account?</p>
            </Link>
            <Button
              level={ButtonLevels.PRIMARY}
              type="submit"
              disabled={loading}
              onClick={onSignIn}
            >
              Sign in
            </Button>
          </div>
          {error && (
            <div>
              <p className="error">{error}</p>
            </div>
          )}
        </form>
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => ({
  signIn: (...args) => dispatch(signIn(...args)),
  clearErrors: () => dispatch(clearErrors()),
});

const mapStateToProps = (state) => ({
  loading: Data.authLoading(state),
  error: Data.authError(state),
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);
