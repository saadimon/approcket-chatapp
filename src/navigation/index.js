import React from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import Route from "./Route";
import Routes from "./routes";

function Navigation({}) {
  return (
    <Router>
      <Switch>
        {Routes.map((route, index) => (
          <Route {...route} key={index} />
        ))}
      </Switch>
    </Router>
  );
}

export default Navigation;
