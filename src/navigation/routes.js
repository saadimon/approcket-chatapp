import Login from "../components/Pages/Authentication/Login";
import Register from "../components/Pages/Authentication/Register";
import Home from "../components/Pages/LoggedIn/Home";
import Links from "./links";

const Routes = [
  {
    path: Links.LOGIN,
    component: Login,
    authenticated: false,
    showIfLoggedIn: false,
  },
  {
    path: Links.REGISTER,
    component: Register,
    authenticated: false,
    showIfLoggedIn: false,
  },
  {
    path: Links.HOME,
    component: Home,
    authenticated: true,
    showIfLoggedIn: true,
  },
  // {
  //   path: Links.ROOM,
  //   component: "",
  //   authenticated: true,
  // },
  {
    path: Links.NOT_FOUND,
    component: () => <></>,
  },
];

export default Routes;
