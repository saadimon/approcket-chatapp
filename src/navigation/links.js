const Links = {
  LOGIN: "/login",
  REGISTER: "/register",
  HOME: "/home",
  ROOM: "/room/:roomId",
  NOT_FOUND: "/",
};

export default Links;
