import React from "react";
import { useEffect } from "react";
import { connect } from "react-redux";
import { Redirect, Route as RouterRoute } from "react-router-dom";
import LoadingIndicator from "../components/Small/LoadingIndicator";
import LayoutContainer from "../components/Layout";
import Data from "../data";
import { loadInitialData } from "../redux/app/actions";
import Links from "./links";
import variables from "../util/variables";

function Route({
  isLoggedIn,
  path,
  authenticated,
  showIfLoggedIn,
  component: Component,
  loadInitialData,
  initialLoading,
  ...props
}) {
  useEffect(() => {
    if (isLoggedIn) {
      loadInitialData();
    }
  }, [isLoggedIn]);

  if (path === Links.NOT_FOUND) {
    return isLoggedIn ? (
      <Redirect to={Links.HOME} />
    ) : (
      <Redirect to={Links.LOGIN} />
    );
  }

  if (authenticated && !isLoggedIn) return <Redirect to={Links.LOGIN} />;

  if (!showIfLoggedIn && isLoggedIn) return <Redirect to={Links.HOME} />;

  if (initialLoading)
    return (
      <div
        className="vh-100 center-items"
        style={{ backgroundColor: variables.colorBackground }}
      >
        <LoadingIndicator />
      </div>
    );

  return authenticated ? (
    <div className="vh-100">
      <LayoutContainer>
        <RouterRoute path={path} {...props}>
          <Component />
        </RouterRoute>
      </LayoutContainer>
    </div>
  ) : (
    <RouterRoute path={path} {...props}>
      <Component />
    </RouterRoute>
  );
}

const mapStateToProps = (state) => ({
  isLoggedIn: Data.isLoggedIn(state),
  initialLoading: Data.getInitialLoading(state),
});

const mapDispatchToProps = (dispatch) => ({
  loadInitialData: (...args) => dispatch(loadInitialData(...args)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Route);
